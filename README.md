# README #

This App is a simple Shabbat Times clock app which includes a smart mechanism for disabling it's main screen during a Good Day (in the Jewish culture).

### What is this repository for? ###

* The app is built upon the Module named [YomTov Blocker](https://bitbucket.org/3pCupsDev/shabbat-clock/src/master/YomTov%20Blocker/)
* Version 1.0.0
* All data is provided by [Heb calc](https://www.hebcal.com)

### Technologies and Dependencies Used ###

* The app is written in Kotlin
* Retrofit & OkHttp for networking
* Composed in a MVVM architecture
* With Timed and Least Recently Used Caching mechanism

### Visit the Website ###

* [3P Cups](https://3p-cups.com)