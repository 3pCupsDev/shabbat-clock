package com.triPCups.calc.yomtovblocker

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import com.triPCups.calc.yomtovblocker.abs.SimpleBlocker
import com.triPCups.calc.yomtovblocker.abs.ViewBlocker
import com.triPCups.calc.yomtovblocker.models.YomTovDate
import com.triPCups.calc.yomtovblocker.repo.YomTovRepo
import com.triPCups.calc.yomtovblocker.repo.YomTovRepoImpl

class YomTovBlocker(
    lifecycleOwner: LifecycleOwner,
    viewGroup: ViewGroup,
    private val blockPlaceholder: View,
) {

    interface YomTovBlockerListener {

        fun onYomTovDataReceived(shabbatClock: View, yomTovDate: YomTovDate)

        /**
         * Indicates that App is now blocked
         */
        fun onYomTovStarted()

        /**
         * Indicates that App is now unblocked
         */
        fun onYomHolStarted()
    }

    var listener: YomTovBlockerListener? = null

    private val service: YomTovRepo = YomTovRepoImpl()
    private val _blocker: SimpleBlocker = SimpleBlocker(viewGroup, ViewBlocker.ViewState.Unblocked)

    init {
        blockPlaceholder.isClickable = true
        blockPlaceholder.isFocusable = true

        service.yomTovData.observe(lifecycleOwner) {
            Log.d("wow", "onYomTovDataReceived: ${it.titleOrig}")
            Log.d("wow", "onYomTovDataReceived: ${it.title}")
            Log.d("wow", "onYomTovDataReceived: ${it.date}")
            Log.d("wow", "onYomTovDataReceived: ${it.category}")
            Log.d("wow", "onYomTovDataReceived: ${it.hebrew}")
            listener?.onYomTovDataReceived(blockPlaceholder, it)
        }

        service.isYomTov.observe(lifecycleOwner) {
            if (it) {
                onYomTov()
            } else {
                onYomHol()
            }
        }
    }

    protected fun onYomTov() {
        _blocker.showBlockView(blockPlaceholder)
    }

    protected fun onYomHol() {
        _blocker.hideBlockView(blockPlaceholder)
    }

    fun isYomTov() : Boolean {
        return _blocker.isBlocked
    }

    fun isYomHol() : Boolean {
        return !_blocker.isBlocked
    }
}