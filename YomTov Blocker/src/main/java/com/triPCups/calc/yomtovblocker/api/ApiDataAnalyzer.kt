package com.triPCups.calc.yomtovblocker.api

import android.text.format.DateUtils
import android.util.Log
import com.triPCups.calc.yomtovblocker.common.checkIsNow
import com.triPCups.calc.yomtovblocker.common.isNow
import com.triPCups.calc.yomtovblocker.common.isToday
import com.triPCups.calc.yomtovblocker.common.parseToDate
import com.triPCups.calc.yomtovblocker.models.YomTovDate

object ApiDataAnalyzer {

    private const val YOM_TOV_START = "candles"
    private const val YOM_TOV_ENDS = "havdalah"

    fun checkIfNowIsYomTov(arrayList: ArrayList<YomTovDate>, callback: (YomTovDate) -> Unit) : Boolean {
        for(yomTov in arrayList){
            if(yomTov.date.contains("T") && yomTov.isToday()) {
                Log.d("wow", "checkIfNowIsYomTov: ${yomTov.date}")
                Log.d("wow", "checkIfNowIsYomTov: ${yomTov.date.parseToDate()}")

                if(yomTov.category == YOM_TOV_START) {
                    Log.d("wow", "start of: ${yomTov.hebrew}")

                    callback.invoke(yomTov)
                    return yomTov.checkIsNow()
                } else if(yomTov.category == YOM_TOV_ENDS) {
                    Log.d("wow", "end of: ${yomTov.hebrew}")

                    callback.invoke(yomTov)
                    return !yomTov.checkIsNow()
                }
            }
        }

        return false
    }
}