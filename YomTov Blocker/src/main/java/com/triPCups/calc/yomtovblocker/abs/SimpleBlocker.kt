package com.triPCups.calc.yomtovblocker.abs

import android.view.View
import android.view.ViewGroup

open class SimpleBlocker(override var viewGroup: ViewGroup?, override var currentState: ViewBlocker.ViewState) :
    ViewBlocker {

    fun showBlockView(blockPlaceholder: View) {
        if (blockPlaceholder.parent != null) {
            (blockPlaceholder.parent as ViewGroup).removeView(blockPlaceholder) // <- fix
        }
        viewGroup?.addView(blockPlaceholder)
        updateState(false)
    }

    fun hideBlockView(blockPlaceholder: View) {
        viewGroup?.removeView(blockPlaceholder)
        updateState(true)
    }

    val isBlocked : Boolean
        get() = currentState == ViewBlocker.ViewState.Blocked// || currentState.isBlocked
}