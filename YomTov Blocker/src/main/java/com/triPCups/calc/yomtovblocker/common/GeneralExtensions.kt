package com.triPCups.calc.yomtovblocker.common

import android.app.Activity
import android.os.Build
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.triPCups.calc.yomtovblocker.YomTovBlocker
import com.triPCups.calc.yomtovblocker.view.SimpleBlockLayout

/**
 * @param blockPlaceholder is a custom UI given to block the view of your app.
 * can be one of ( SimpleBlockLayout, ShabbatBlockLayout, or BeautifulShabbatBlockLayout )
 * @param listener YomTovBlocker.YomTovBlockerListener callback methods for connecting fragment with data from repo
 */
fun Fragment.addShabbatClock(blockPlaceholder: View = SimpleBlockLayout(requireContext()), listener: YomTovBlocker.YomTovBlockerListener? = null) {
    YomTovBlocker(viewLifecycleOwner,view?.parent as ViewGroup, blockPlaceholder= blockPlaceholder).apply {
        this.listener = listener
    }
}

fun AppCompatActivity.addShabbatClock(blockPlaceholder: View = SimpleBlockLayout(this), listener: YomTovBlocker.YomTovBlockerListener? = null) {
    supportFragmentManager.fragments.last().addShabbatClock(blockPlaceholder, listener)
}

@RequiresApi(Build.VERSION_CODES.O)
fun Activity.addShabbatClock(blockPlaceholder: View = SimpleBlockLayout(this), listener: YomTovBlocker.YomTovBlockerListener? = null) {
    (fragmentManager.fragments?.last() as Fragment?)?.addShabbatClock(blockPlaceholder, listener)
}