package com.triPCups.calc.yomtovblocker.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class YomTovDate(
    @Expose
    @SerializedName("title")
    val title: String,
    @Expose
    @SerializedName("date")
    val date: String,
    @Expose
    @SerializedName("category")
    val category: String,
    @Expose
    @SerializedName("title_orig")
    val titleOrig: String,
    @Expose
    @SerializedName("hebrew")
    val hebrew: String
)


data class ApiResults(val items: ArrayList<YomTovDate>)