package com.triPCups.calc.yomtovblocker.abs

import android.view.ViewGroup

interface ViewBlocker {

    var viewGroup: ViewGroup?

    var currentState: ViewState


    fun updateState(isBlocked: Boolean) {
        currentState = if (isBlocked) {
            ViewState.Blocked
        } else {
            ViewState.Unblocked
        }
    }


    sealed class ViewState(var isBlocked: Boolean) {
        object Unblocked : ViewState(false)
        object Blocked : ViewState(true)
    }

}