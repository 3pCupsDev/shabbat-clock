package com.triPCups.calc.yomtovblocker.api

import com.triPCups.calc.yomtovblocker.models.ApiResults
import com.triPCups.calc.yomtovblocker.models.YomTovDate
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("shabbat?cfg=json")
    fun getNextYomTovs(): Call<ApiResults>

}