package com.triPCups.calc.yomtovblocker.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.triPCups.calc.yomtovblocker.api.ApiDataAnalyzer
import com.triPCups.calc.yomtovblocker.api.ApiFetcher
import com.triPCups.calc.yomtovblocker.models.YomTovDate

class YomTovRepoImpl : YomTovRepo {

    override val yomTovData: MutableLiveData<YomTovDate> = MutableLiveData<YomTovDate>()

    override val isYomTov: LiveData<Boolean> = MutableLiveData(false).apply {
        ApiFetcher.fetch {
            val isYomTov = if (!it.items.isNullOrEmpty()) {
                ApiDataAnalyzer.checkIfNowIsYomTov(it.items) { yomTov ->
                    yomTovData.postValue(yomTov)
                }
            } else {
                false
            }
            postValue(isYomTov)
        }
    }
}