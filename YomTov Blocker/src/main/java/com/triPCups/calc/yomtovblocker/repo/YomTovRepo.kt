package com.triPCups.calc.yomtovblocker.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.triPCups.calc.yomtovblocker.models.YomTovDate

interface YomTovRepo {

    val yomTovData: MutableLiveData<YomTovDate>

    val isYomTov: LiveData<Boolean>
}