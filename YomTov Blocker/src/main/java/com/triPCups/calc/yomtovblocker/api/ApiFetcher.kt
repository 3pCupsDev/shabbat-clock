package com.triPCups.calc.yomtovblocker.api

import android.util.Log
import com.triPCups.calc.yomtovblocker.models.ApiResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object ApiFetcher {

    //todo add LruCache FROM 3P CUPS ESSENTIALS lib

    fun fetch(callback: (ApiResults)-> Unit) {
        ApiClient.getClient.getNextYomTovs().enqueue(object : Callback<ApiResults> {

            override fun onResponse(call: Call<ApiResults>?, response: Response<ApiResults>?) {
                if(response?.isSuccessful == true && !response.body()?.items.isNullOrEmpty()) {
                    callback(response.body()!!)
                }
            }

            override fun onFailure(call: Call<ApiResults>?, t: Throwable?) {
                Log.e("wow", "onFailure: ${t.toString()}", )
            }
        })
    }
}