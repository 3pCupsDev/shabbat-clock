package com.triPCups.calc.yomtovblocker.common

import android.annotation.SuppressLint
import android.text.format.DateUtils
import com.triPCups.calc.yomtovblocker.models.YomTovDate
import java.text.SimpleDateFormat
import java.util.*


fun Date.isNow(): Boolean {
    return before(Date())
}

@SuppressLint("SimpleDateFormat")
fun String.parseToDate(): Date? {
    val sf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    return sf.parse(this)
}

fun YomTovDate.checkIsNow() : Boolean {
    return date.parseToDate()?.isNow() == true
}



fun YomTovDate.isToday() : Boolean {
    val date = date.parseToDate()
    return DateUtils.isToday(date?.time?: 0)
}