package com.triPCups.calc.shabbatClock.ui.shabbat_block

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.triPCups.calc.shabbatClock.R


class ShabbatBlockLayout : FrameLayout {
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!, attrs, defStyle
    ) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        initView()
    }

    constructor(context: Context?) : super(context!!) {
        initView()
    }

    private fun initView() {
        inflate(context, R.layout.shabbat_block_layout, this)
    }
}