package com.triPCups.calc.shabbatClock.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.triPCups.calc.shabbatClock.R
import com.triPCups.calc.shabbatClock.ui.shabbat_block.BeautifulShabbatBlockLayout
import com.triPCups.calc.yomtovblocker.YomTovBlocker
import com.triPCups.calc.yomtovblocker.common.addShabbatClock
import com.triPCups.calc.yomtovblocker.models.YomTovDate

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private val webView: WebView?
        get() = view?.findViewById<WebView>(R.id.web_view)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initObservers()
        initWebView()
        addShabbatClock(BeautifulShabbatBlockLayout(requireContext()), shabbatListenerClock)
    }

    private val shabbatListenerClock: YomTovBlocker.YomTovBlockerListener =
        object : YomTovBlocker.YomTovBlockerListener {
            override fun onYomTovDataReceived(shabbatClock: View, yomTovDate: YomTovDate) {
                (shabbatClock as BeautifulShabbatBlockLayout).bind(yomTovDate)
            }

            override fun onYomTovStarted() {
                Toast.makeText(
                    context,
                    "האפליקציה כשרה ולכן נסגרת בימי חג, אנא חזרו מאוחר יותר",
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onYomHolStarted() {}
        }

    private fun initObservers() {
        viewModel.currentUrl.observe(viewLifecycleOwner) {
            webView?.loadUrl(it)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        webView?.apply {
            settings.apply {
                javaScriptEnabled = true
                displayZoomControls = true
                zoomBy(1.6f)
            }
        }
    }

}