package com.triPCups.calc.shabbatClock.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    companion object {
        const val WEB_VIEW_URL: String = "https://the-3p-cups-project.web.app/shabbat/index.html"
    }


    val currentUrl: LiveData<String> = MutableLiveData<String>().apply {
        postValue(WEB_VIEW_URL)
    }

}