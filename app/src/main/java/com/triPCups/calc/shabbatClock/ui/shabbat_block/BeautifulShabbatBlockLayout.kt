package com.triPCups.calc.shabbatClock.ui.shabbat_block

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import com.triPCups.calc.shabbatClock.R
import com.triPCups.calc.yomtovblocker.models.YomTovDate


class BeautifulShabbatBlockLayout : FrameLayout {
    private lateinit var eventNameTv: TextView
    private lateinit var eventTimeTv: TextView

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!, attrs, defStyle
    ) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        initView()
    }

    constructor(context: Context?) : super(context!!) {
        initView()
    }

    private fun initView() {
        inflate(context, R.layout.beautiful_shabbat_block_layout, this)
        bindViews() //todo use view binding
    }

    private fun bindViews() {
        eventNameTv = rootView.findViewById<TextView>(R.id.event_name)
        eventTimeTv = rootView.findViewById<TextView>(R.id.event_time)
    }

    fun bind(data: YomTovDate) {
        eventNameTv.text = data.hebrew
        eventTimeTv.text = data.title
    }
}